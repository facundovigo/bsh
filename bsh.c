#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <sys/wait.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <wordexp.h>
#include <limits.h>



#define PROMPT "# "
#define NELEMS(x)  (sizeof(x) / sizeof(x[0]))

const char* BUILT_IN[] = {"","exit","mycd","mypwd","export", "getenv"};

extern char **environ;

int cmd_prompt(char*, wordexp_t*);
void free_input(char*, wordexp_t*);
void exec_builtin(int, char*[]);
void exec_cmd(char*[]);
int is_builtin(char*[]);
void builtin_export(char* path);
/*
 *Mi FUNCION
*/

void  handler_myhandler(){
	printf ("Me Salio WIIIII" );
	signal(SIGINT, handler_myhandler);
}

char* promt(){
	char* destino = malloc(sizeof(char)*225);
	char* string1 = getcwd(destino,255);
	char string2[] = "#:" ;
	return strcat( string1 , string2 );	
} 

void builtin_export(char* path){
	
	
	if (path == NULL) // si no pasa parametros entonces muestro las variables del entorno
	{
		char **env = environ; 
		while ( *env != NULL ) 		
	{
		printf("%s\n",*env);
		env++;
	}
		env = NULL;
	}
	else
		putenv(path);

}

/*
 * Function: main
 * --------------
 * Main command loop.
 */
int main(int argc, char* argv[])
{
    char* input_str = NULL;
    int cmd = 0;
     wordexp_t cmd_argv;

    signal(SIGINT, handler_myhandler);
  
    while(1)
    {
        if(!cmd_prompt(input_str, &cmd_argv))
        {
            free_input(input_str, &cmd_argv);
            continue;
        }

        if ((cmd = is_builtin(cmd_argv.we_wordv)))
            exec_builtin(cmd, cmd_argv.we_wordv);   // exec built_in command
        else
            exec_cmd(cmd_argv.we_wordv);            // exec command

        free_input(input_str, &cmd_argv);
    }
}

/*
 * Function:  cmd_prompt
 * -----------------------
 * Handles command line input using readline and wordexp.
 * Prints the prompt and waits for user's input returning 0 if no command was
 * issued, or 1 otherwise with cmd_argv holding the parsed command line.
 */
int cmd_prompt(char* input_str, wordexp_t* cmd_argv)
{
    if(!(input_str = readline(promt())))     // command input
    {
        printf("exit\n");
        exit(0);
    }

    int error = wordexp(input_str, cmd_argv, WRDE_SHOWERR | WRDE_UNDEF);

    if(error)
    {
        switch (error) {
            case WRDE_BADCHAR:
                printf("Bad character. Illegal occurrence of newline or one of |, &, ;, <, >, (, ), {, }\n");
                break;
            case WRDE_BADVAL:
                printf("Undefined variable\n");
                break;
            case WRDE_SYNTAX:
                printf("Syntax error\n");
                break;
            default:
                printf("Unknown error\n");
        }

         return 0;
    }

    if (cmd_argv->we_wordc == 0) // no input
    {
        return 0;
    }

    add_history(input_str);

    return 1;
}

/*
 * Function: exec_cmd
 * ------------------
 * Execute given command and waits till it's completed. cmd_argv[0] must shields
 * the command to be executed, cmd_argv[i] (i > 0) shields the command arguments.
 */
void exec_cmd(char* cmd_argv[])
{
    int status;
    int child_pid = fork();

    if (child_pid != 0) // parent process
    {
        waitpid(child_pid, &status, 0);
    }
    else // child process
    {
        execvp(cmd_argv[0], cmd_argv); // execute PROGRAM
        perror("");
        exit(0);
    }
}

/*
 * Function: exec_builtin
 * ----------------------
 * Executes built-in commmands registered in BUILT_IN array.
 */
void exec_builtin(int cmd, char* cmd_argv[])
{
    char path[255];

/*
cmd_argv = {"export" , 
			"a=b"}
*/

    switch(cmd)
    {
        case 1:
            exit(0);
            break;
        case 2:
            chdir(cmd_argv[1]);
            break;
        case 3:
            getcwd(path, 255);
            printf("%s\n", path);
            break;
        case 4:
        	builtin_export(cmd_argv[1]);
        	break;
        case 5: 
        	printf("%s\n", getenv(cmd_argv[1]));
        default:
            printf("-bsh: command not found\n");
    }

    return;
}

/*
 * Function: is_builtin
 * ----------------------
 * Finds out if the command is a built-in one, registered in BUILT_IN array and
 * returns the command position.
 */
int is_builtin(char* cmd_argv[])
{
    for(int i = 1; i < NELEMS(BUILT_IN); i++)
        if (!strcmp(cmd_argv[0], BUILT_IN[i]))
            return i;

    return 0;
}


// utility functions
void free_input(char* cmd_str, wordexp_t* cmd_argv)
{
    free(cmd_str);
    wordfree(cmd_argv);
}
